# Text classification
Lithuanian language text classification. Main steps are

### Prepossessing:

* Tokenization
* Stop words removing using helper nlp_helper/stop_words.csv
* Stemming using helper npl_helper/stop_words.csv

### BagOfwords matrix creation:

* rows are document id
* cols are tokens
* values are tf-idf

### Model testing:

* create tokens vector for unseen document where values are (1 or 0).
* calculate distance between BagOfwords matrix and new document tokens vector.
* Predict class using k nearest neighbors.

### Model evaluation

![GitHub Logo](/graphical_outputs/confussion_matrix.png)


# Documents clustering


# Latent Semantic Analysis(LSA)
Simple topics extraction model. Main idea is to apply Singular Value Decomposition(SVD) algorithm to BagOfWords matrix M and find composition M = USV^T. Topics are extracted from left hand side matrix U where first components explains the highest amount of variance in origin BagOfWords matrix.

### Preprocessing:

* BagOfWords matrix normalization (columns normalized using Euclidean distance). It helps to equalize variance across all components

### Topics extraction:

* Apply SVD to BagOfWords and extract topics from Matrix U^T (dimensions x tokens_numb), where topics are rows (weigt, token) combination.

### Visualize topics:

![GitHub Logo](/graphical_outputs/1_component_lsa.png)

![GitHub Logo](/graphical_outputs/2_component_lsa.png)

![GitHub Logo](/graphical_outputs/3_component_lsa.png)

![GitHub Logo](/graphical_outputs/4_component_lsa.png)

# Search query

Search is based on http://webhome.cs.uvic.ca/~thomo/svd.pdf article, where lsa is applied to find related documents using not only exact query similarities, but deeper relations between documents.
![GitHub Logo](/graphical_outputs/search_query_example.png)
### Example
Query = "švietim apdovanojam"

Result:

* ['Imasi mokslininkų algų: siūlo kelti iki 50 proc.']
* ['Įteiktos 6 Mokslo premijos']
* ['Lietuvoje į susitikimą kviečia Nobelio premijos laureatas']
* ['100 tūkst. eurų išdalins populiarinantiems mokslą']
* ['V. Vaičaitis. Konkursinis mokslo finansavimas ar pasityčiojimas iš mokslininkų?']