# -*- coding: utf-8 -*-
"""
Created on Wed May 10 15:16:16 2017

@author: minven2
"""

from sklearn.preprocessing import normalize
import pickle
import numpy as np

from sklearn.metrics.pairwise import cosine_similarity
from sklearn.utils.extmath import randomized_svd
from sklearn.cluster import KMeans
from scipy.cluster.hierarchy import ward, dendrogram
import matplotlib.pyplot as plt
import pandas as pd
class DocumentsClustering(object):
    
    def __init__(self, dimensions):
        self.dimensions = dimensions
        self.bag_of_words_df = self._load_pickle("bag_of_words_matrix.p")
        self.features = list(self.bag_of_words_df.columns)
        self.documents_mapping = dict(zip(list(range(len(self.bag_of_words_df))),
                                          list(self.bag_of_words_df.index)))
        self.document_ids_train = self._load_pickle("document_ids_train.p")
        # https://stats.stackexchange.com/questions/69157/why-do-we-need-to-normalize-data-before-analysis
        self.bag_of_words_matrix = normalize(self.bag_of_words_df.as_matrix())
        self.bag_of_words_matrix_reduced = np.array([])
        self.U = np.array([])
        self.S = np.array([])
        self.V = np.array([])
        self.B = np.array([])
        
    
        #self.bag_of_words_matrix = self.bag_of_words_df.as_matrix()
        self.components = []
        
    def _load_pickle(self, pickle_name):
        pickle_obj = pickle.load( open( "pickles/{}".format(pickle_name), "rb" ))       
        return pickle_obj
    
    def get_related(self, bag_of_words_matrix, document_id, N=10):
        try:
            document_scores = bag_of_words_matrix[document_id,:]
        except IndexError as e:
            raise RuntimeError("document id doesn't exists") from e
        else:            
            similarities = bag_of_words_matrix.dot(document_scores)
            # sort similarities and extract index from topN similarities
            # also remove biggest similarity between document itself
            best = np.argsort(similarities)[::-1][:N]
            similarities = sorted(zip(best, similarities[best]), key=lambda x: -x[1])
            similarities_mapped = [(self.documents_mapping[x[0]],x[1]) for x in similarities]
            print(similarities_mapped)

    def RandomizedSVD(self):
        # http://stackoverflow.com/questions/31523575/get-u-sigma-v-matrix-from-truncated-svd-in-scikit-learn
        U, S, V = randomized_svd(self.bag_of_words_matrix.T, 
                                      n_components=self.dimensions,
                                      n_iter=5,
                                      random_state=None)
        self.U = U
        self.S = S
        self.V = V
        self.documents_representation = (np.diag(S) * np.matrix(V)).T

 
    
    def SVD(self):
        ## https://github.com/josephwilk/semanticpy/blob/master/semanticpy/transform/lsa.py
        bag_of_words_matrix = self.bag_of_words_matrix
        rows,cols = self.bag_of_words_matrix.shape
        U, S, V = np.linalg.svd(bag_of_words_matrix, full_matrices=True)
        self.U = U
        self.S = S
        self.V = V
        new_U = U[:,:self.dimensions]
        new_S = S[:self.dimensions]
        new_V = V[:,:self.dimensions]
        self.U = new_U
        self.S = new_S
        self.V = new_V
        self.B = np.matrix(U[:, :self.dimensions]) * np.diag(S[:self.dimensions])


    
if __name__ == "__main__":
    documents_clusterin = DocumentsClustering(80)

    documents_ids_training = documents_clusterin.document_ids_train
    documents_mapping = documents_clusterin.documents_mapping
    bag_of_words_matrix = documents_clusterin.bag_of_words_matrix

    documents_clusterin.RandomizedSVD()
    U = documents_clusterin.U
    S = documents_clusterin.S
    V = documents_clusterin.V
    documents_representation = documents_clusterin.documents_representation    
   
    dist = 1 - cosine_similarity(documents_representation)
   

    num_clusters = 10
    km = KMeans(n_clusters=num_clusters)
    km.fit(documents_representation)
    clusters_pred = km.labels_.tolist()
    clusters_true = list(documents_ids_training.values())
    
    
    confusion_matrix_ini = np.zeros((len(set(clusters_true)), len(set(clusters_pred))))
    
    confusion_matrix = pd.DataFrame(data=confusion_matrix_ini,
                                    index=set(clusters_true),
                                    columns=set(clusters_pred))
    
    for true_cluster, pred_cluster in zip(clusters_true, clusters_pred):
        print(true_cluster, pred_cluster)        
        new_value = confusion_matrix[pred_cluster][true_cluster] + 1
        confusion_matrix.set_value(true_cluster, pred_cluster, new_value)        

#
#    linkage_matrix = ward(documents_representation) #define the linkage_matrix using ward clustering pre-computed distances
#    fig, ax = plt.subplots(figsize=(25, 40)) # set size
#    ax = dendrogram(linkage_matrix, orientation="right");
#    plt.tick_params(\
#        axis= 'x',          # changes apply to the x-axis
#        which='both',      # both major and minor ticks are affected
#        bottom='off',      # ticks along the bottom edge are off
#        top='off',         # ticks along the top edge are off
#        labelbottom='off')
#    
#    plt.tight_layout() #show plot with tight layout
#    
#    plt.savefig('graphical_outputs/ward_clusters.png', dpi=200) #save figure as ward_clusters


